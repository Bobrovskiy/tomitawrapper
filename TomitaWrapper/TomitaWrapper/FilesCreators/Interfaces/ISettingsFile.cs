﻿namespace TomitaWrapper.FilesCreators.Interfaces {

    /// <summary>
    ///   Базовая информация о файле
    /// </summary>
    internal interface ISettingsFile {
        /// <summary>
        ///   Имя файла, который должен быть создан
        /// </summary>
        string FileName { get; }


    }

}