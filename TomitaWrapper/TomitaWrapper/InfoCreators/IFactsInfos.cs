﻿namespace TomitaWrapper.InfoCreators {

    /// <summary>
    ///   Interface for create informations about facts
    /// </summary>
    internal interface IFactsInfos {
        /// <summary>
        ///   Crteate facts files informations (Файл facttypes.proto)
        /// </summary>
        /// <param name="factName">Name of set file of facts</param>
        /// <param name="factsNames">facts names</param>
        /// <returns>full path of file</returns>
        string Create(string factName, string[] factsNames);
    }

}