﻿namespace TomitaWrapper.InfoCreators {

    /// <summary>
    ///   Interface for create total config file (kwtypes_my.proto)
    /// </summary>
    internal interface ICreateConfigInfos {}

}